# My project's README

This code implements a ADMM algorithm for fitting multivariate convex Threshold Autoregressive (TAR) models. 
* two group lasso penalties are used for structural break estimation and model sparsity
* to solve the main quadratic optmization problem a Kalman Filtering-Smoothing algorithm is employed which leads to linear complexity.
