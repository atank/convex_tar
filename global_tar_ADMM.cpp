#include <RcppArmadillo.h>
#include <Rcpp.h>
#include <RcppArmadilloExtensions/sample.h>
using namespace Rcpp;

// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
int timesTwo(int x) {
   return x * 2;
}

void showValue_double(double x) {
   Rcout << "The value is " << x << std::endl;
}

// [[Rcpp::depends(RcppArmadillo)]]


arma::mat matrix_group_soft_threshold(arma::mat x, double kappa) {
  double x_norm = arma::norm(x,"fro");
  return(std::max(0.0,1.0 - kappa/x_norm)*x);
}

// [[Rcpp::export]]
arma::mat fused_group_lasso_theta_cpp(arma::mat theta, arma::mat u_y, double kappa, int p, int lag, int N_tilde) {
  arma::mat delta = theta + u_y;
  for (int i = 2; i <= N_tilde; i++) {
    int index_start = p*lag*(i-1);
    int index_end = p*lag*i - 1;
    delta.cols(index_start,index_end) = matrix_group_soft_threshold(delta.cols(index_start,index_end),kappa); 
  }
  return(delta);
} 


// [[Rcpp::export]]
arma::rowvec kalman_smoother_runner_cpp(arma::mat b, double rho, arma::vec x, arma::mat H, int p, int N, bool one_pen) {
  arma::mat eye(p,p,arma::fill::eye);
  arma::mat xhat_t1(N,p,arma::fill::zeros);
  arma::mat xhat_t_t(N,p,arma::fill::zeros);
  arma::mat xhat_t_n(N,p,arma::fill::zeros);
  arma::cube Cov_t1(p,p,N,arma::fill::zeros);
  arma::cube Cov_t_t(p,p,N,arma::fill::zeros);
  arma::cube Cov_t_n(p,p,N,arma::fill::zeros);
    
  double q_weight = 2.0;
  if (one_pen) {
    q_weight = 2.0;
  } else {
    q_weight = 1.0;
  }
  
  for (int i = 0; i < N; i++) {
    if (i > 0) {
      xhat_t1.row(i) = xhat_t_t.row(i-1) + b.row(i);
      Cov_t1.slice(i) = Cov_t_t.slice(i-1) + eye*(q_weight/rho);
    } else {
      xhat_t1.row(i) = b.row(i);
      Cov_t1.slice(i) = eye*(q_weight/rho);
    }

    double tildey = x(i) - arma::dot(H.row(i),xhat_t1.row(i));

    double S = 1.0 + arma::dot(H.row(i),Cov_t1.slice(i)*H.row(i).t());

    arma::colvec K = Cov_t1.slice(i)*H.row(i).t()/S;
    xhat_t_t.row(i) = xhat_t1.row(i) + K.t()*tildey;
    Cov_t_t.slice(i) = (eye - K*H.row(i))*Cov_t1.slice(i);
  }
  xhat_t_n.row(N-1) = xhat_t_t.row(N-1);
  Cov_t_n.slice(N-1) = Cov_t_t.slice(N-1);
  
  for (int i = N-2; i >= 0; i--) {
    arma::mat Ck = Cov_t_t.slice(i) * inv(Cov_t1.slice(i+1));
    xhat_t_n.row(i) = xhat_t_t.row(i) + arma::trans(Ck*(xhat_t_n.row(i+1).t() - xhat_t1.row(i+1).t()));
    Cov_t_n.slice(i) = Cov_t_t.slice(i) + Ck*Cov_t_n.slice(i+1) - Cov_t1.slice(i)*Ck.t();
  }
    
  return(arma::vectorise(xhat_t_n,1));
}